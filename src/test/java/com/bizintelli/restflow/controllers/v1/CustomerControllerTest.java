package com.bizintelli.restflow.controllers.v1;

import com.bizintelli.restflow.api.v1.model.CustomerDTO;
import com.bizintelli.restflow.api.v1.model.CustomerListDTO;
import com.bizintelli.restflow.respositories.CustomerRepository;
import com.bizintelli.restflow.services.CustomerService;
import com.bizintelli.restflow.services.ResourceNotFoundException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class CustomerControllerTest extends AbstractRestControllerTest {



    public static final long ID1 = 1L;
    public static final String FIRSTNAME1 = "John";
    public static final String LASTNAME1 = "Li";
    public static final String CUSTOMERURL1 = CustomerController.BASE_URL + "/1";

    public static final long ID2 = 2L;
    public static final String FIRSTNEME2 = "Grace";
    public static final String LASTNAME2 = "Wang";
    public static final String CUSTOMERURL2 = CustomerController.BASE_URL + "/2";


    @Mock
    CustomerService customerService;

    @InjectMocks
    CustomerController customerController;

    MockMvc mockMvc;
    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(customerController).setControllerAdvice(new RestResponseExceptionHandler()).build();
    }

    @Test
    public void getAllCustomers() throws Exception {

        CustomerDTO customerDTO1= new CustomerDTO();
        customerDTO1.setId(ID1);
        customerDTO1.setFirstname(FIRSTNAME1);
        customerDTO1.setLastname(LASTNAME1);
        customerDTO1.setCustomerURL(CUSTOMERURL1);

        CustomerDTO customerDTO2= new CustomerDTO();
        customerDTO2.setId(ID2);
        customerDTO2.setFirstname(FIRSTNEME2);
        customerDTO2.setLastname(LASTNAME2);
        customerDTO2.setCustomerURL(CUSTOMERURL2);

        List<CustomerDTO> customerListDTO = Arrays.asList(customerDTO1,customerDTO2);

        when(customerService.getCustomers()).thenReturn(customerListDTO);

        mockMvc.perform(get(CustomerController.BASE_URL).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.customers", hasSize(2)));


    }

    @Test
    public void getCustomerById() throws Exception {
        CustomerDTO customerDTO1= new CustomerDTO();
        customerDTO1.setId(ID1);
        customerDTO1.setFirstname(FIRSTNAME1);
        customerDTO1.setLastname(LASTNAME1);
        customerDTO1.setCustomerURL(CUSTOMERURL1);


        when(customerService.getCustomerById(anyLong())).thenReturn(customerDTO1);

        mockMvc.perform(get(CustomerController.BASE_URL+"/1").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.firstname",equalTo(FIRSTNAME1)))
                .andExpect((jsonPath("$.lastname",equalTo(LASTNAME1))));

    }

    @Test
    public void createCustomer() throws Exception {
        //given
        CustomerDTO customerDTO= new CustomerDTO();
        customerDTO.setId(ID1);
        customerDTO.setFirstname(FIRSTNAME1);
        customerDTO.setLastname(LASTNAME1);
        customerDTO.setCustomerURL(CUSTOMERURL1);

        CustomerDTO returnCustomerDTO= new CustomerDTO();
        returnCustomerDTO.setId(customerDTO.getId());
        returnCustomerDTO.setFirstname(customerDTO.getFirstname());
        returnCustomerDTO.setLastname(customerDTO.getLastname());
        returnCustomerDTO.setCustomerURL(customerDTO.getCustomerURL());

        when(customerService.createCustomer(customerDTO)).thenReturn(returnCustomerDTO);

        //then
        mockMvc.perform(post(CustomerController.BASE_URL).contentType(MediaType.APPLICATION_JSON).content(asJsonString(customerDTO)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.firstname",equalTo(FIRSTNAME1)))
                .andExpect(jsonPath("$.lastname",equalTo(LASTNAME1)))
                .andExpect(jsonPath("$.customer_url", equalTo(CUSTOMERURL1)));

//        String response = mockMvc.perform(post("/api/v1/customers/").contentType(MediaType.APPLICATION_JSON).content(asJsonString(customerDTO)))
//                .andExpect(status().isCreated())
//                .andReturn().getResponse().getContentAsString();
//        System.out.println(response);
    }

    @Test
    public void saveCustomer() throws Exception {
        //given
        CustomerDTO customerDTO= new CustomerDTO();
        customerDTO.setId(ID1);
        customerDTO.setFirstname(FIRSTNAME1);
        customerDTO.setLastname(LASTNAME1);
        customerDTO.setCustomerURL(CUSTOMERURL1);

        CustomerDTO returnCustomerDTO= new CustomerDTO();
        returnCustomerDTO.setId(customerDTO.getId());
        returnCustomerDTO.setFirstname(customerDTO.getFirstname());
        returnCustomerDTO.setLastname(customerDTO.getLastname());
        returnCustomerDTO.setCustomerURL(customerDTO.getCustomerURL());

        when(customerService.saveCustomerById(anyLong(),any(CustomerDTO.class))).thenReturn(returnCustomerDTO);

        //then
        mockMvc.perform(put(CustomerController.BASE_URL+"/" + ID1).contentType(MediaType.APPLICATION_JSON).content(asJsonString(customerDTO)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.firstname",equalTo(FIRSTNAME1)))
                .andExpect(jsonPath("$.lastname",equalTo(LASTNAME1)))
                .andExpect(jsonPath("$.customer_url", equalTo(CUSTOMERURL1)));
    }

    @Test
    public void patchCustomer() throws Exception {
        //given
        CustomerDTO customerDTO= new CustomerDTO();
        customerDTO.setId(ID1);
        customerDTO.setFirstname(FIRSTNAME1);
        customerDTO.setLastname(LASTNAME1);
        customerDTO.setCustomerURL(CUSTOMERURL1);

        CustomerDTO returnCustomerDTO= new CustomerDTO();
        returnCustomerDTO.setId(customerDTO.getId());
        returnCustomerDTO.setFirstname(customerDTO.getFirstname());
        returnCustomerDTO.setLastname(customerDTO.getLastname());
        returnCustomerDTO.setCustomerURL(customerDTO.getCustomerURL());

        when(customerService.patchCustomerById(anyLong(),any(CustomerDTO.class))).thenReturn(returnCustomerDTO);

        //then
        mockMvc.perform(patch(CustomerController.BASE_URL+"/" + ID1).contentType(MediaType.APPLICATION_JSON).content(asJsonString(customerDTO)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.firstname",equalTo(FIRSTNAME1)))
                .andExpect(jsonPath("$.lastname",equalTo(LASTNAME1)))
                .andExpect(jsonPath("$.customer_url", equalTo(CUSTOMERURL1)));
    }

    @Test
    public void deleteCustomer() throws  Exception {

        mockMvc.perform(delete(CustomerController.BASE_URL+"/" + ID1).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        verify(customerService,times(1)).deleteCustomerById(anyLong());
    }

    @Test
    public void testNotFoundException() throws Exception {

        when(customerService.getCustomerById(anyLong())).thenThrow(ResourceNotFoundException.class);

        mockMvc.perform(get(CustomerController.BASE_URL + "/222")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }
}