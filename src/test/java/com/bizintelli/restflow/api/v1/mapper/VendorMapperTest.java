package com.bizintelli.restflow.api.v1.mapper;

import com.bizintelli.restflow.api.v1.model.VendorDTO;
import com.bizintelli.restflow.domain.Vendor;
import org.junit.Test;

import static org.junit.Assert.*;

public class VendorMapperTest {

    private final Long ID=1L;
    private final String NAME="Vendor Name";

    VendorMapper vendorMapper = VendorMapper.INSTANCE;

    @Test
    public void vendorToVendorDTO() throws Exception {
        //given
        Vendor vendor =new Vendor();
        vendor.setId(ID);
        vendor.setName(NAME);

        VendorDTO vendorDTO = new VendorDTO();
        vendorDTO.setId(ID);
        vendorDTO.setName(NAME);

        //when
        VendorDTO returnedVendorDTO = vendorMapper.vendorToVendorDTO(vendor);

        //then
        assertEquals(vendor.getId(),returnedVendorDTO.getId());
        assertEquals(vendor.getName(),returnedVendorDTO.getName());
        assertNull(vendorDTO.getVendorURL());
    }

    @Test
    public void vendorDTOtoVendor() throws Exception {
        //given
        Vendor vendor =new Vendor();
        vendor.setId(ID);
        vendor.setName(NAME);

        VendorDTO vendorDTO = new VendorDTO();
        vendorDTO.setId(ID);
        vendorDTO.setName(NAME);

        //when
        Vendor  returnedVendor = vendorMapper.vendorDTOtoVendor(vendorDTO);

        //then
        assertEquals(vendorDTO.getId(),returnedVendor.getId());
        assertEquals(vendorDTO.getName(),returnedVendor.getName());
    }

}