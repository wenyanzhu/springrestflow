package com.bizintelli.restflow.api.v1.mapper;

import com.bizintelli.restflow.api.v1.model.CustomerDTO;
import com.bizintelli.restflow.domain.Customer;
import org.junit.Test;

import static org.junit.Assert.*;

public class CustomerMapperTest {

    private final long ID= 1L;
    private final String FIRSTNAME="Tony";
    private final String LASTNAME="Wong";

    CustomerMapper customerMapper=CustomerMapper.INSTANCE;

    @Test
    public void customerToCustomerDTO() throws Exception {
        //give
          Customer customer = new Customer();
          customer.setId(ID);
          customer.setFirstname(FIRSTNAME);
          customer.setLastname(LASTNAME);

        //when
        CustomerDTO customerDTO = customerMapper.customerToCustomerDTO(customer);

        //then
        assertEquals(Long.valueOf(ID), customerDTO.getId());
        assertEquals(FIRSTNAME, customerDTO.getFirstname());
        assertEquals(LASTNAME,customerDTO.getLastname());
    }

    @Test
    public void customerDTOToCustomer() throws Exception {
        //give
        CustomerDTO customerDTO = new CustomerDTO();
        customerDTO.setId(ID);
        customerDTO.setFirstname(FIRSTNAME);
        customerDTO.setLastname(LASTNAME);

        //when
        Customer customer= customerMapper.customerDtoToCustomer(customerDTO);

        //then
        assertEquals(Long.valueOf(ID), customer.getId());
        assertEquals(FIRSTNAME, customer.getFirstname());
        assertEquals(LASTNAME,customer.getLastname());
    }

}