package com.bizintelli.restflow.services;

import com.bizintelli.restflow.api.v1.mapper.CustomerMapper;
import com.bizintelli.restflow.api.v1.model.CustomerDTO;
import com.bizintelli.restflow.domain.Customer;
import com.bizintelli.restflow.respositories.CustomerRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class CustomerServiceImplTest {


    public static final long ID1 = 1L;
    private final Long ID = 1L;
    private final String FIRSTNAME = "Wilson";
    private final String LASTNAME ="Tan";

    CustomerService customerService;

    @Mock
    CustomerRepository customerRepository;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        customerService=new CustomerServiceImpl(CustomerMapper.INSTANCE,customerRepository );
    }

    @Test
    public void getCustomers() throws Exception {
        //given
         List<Customer> customerList = Arrays.asList(new Customer(),new Customer(),new Customer());

         when(customerRepository.findAll()).thenReturn(customerList);

        //when
        List<CustomerDTO> customerDTOS = customerService.getCustomers();

        //then
        assertEquals(3, customerDTOS.size());
    }

    @Test
    public void getCustomerById() throws Exception {

        //given
         Customer customer = new Customer();
         customer.setId(ID);
         customer.setFirstname(FIRSTNAME);
         customer.setLastname(LASTNAME);

         when(customerRepository.findById(anyLong())).thenReturn(java.util.Optional.ofNullable(customer));

        //when
         CustomerDTO customerDTO = customerService.getCustomerById(ID);

        //then
        assertEquals(Long.valueOf(ID),customerDTO.getId());
        assertEquals(FIRSTNAME, customerDTO.getFirstname());
        assertEquals(LASTNAME,customerDTO.getLastname());
    }

    @Test
    public void createCustomer() throws Exception {
        //given
        CustomerDTO customerDTO = new CustomerDTO();
        customerDTO.setFirstname(FIRSTNAME);
        customerDTO.setLastname(LASTNAME);
        customerDTO.setCustomerURL("/api/v1/customers/1");

        Customer savedCustomer = new Customer();
        savedCustomer.setFirstname(customerDTO.getFirstname());
        savedCustomer.setLastname(customerDTO.getLastname());;
        savedCustomer.setId(ID1);

        when(customerRepository.save(any(Customer.class))).thenReturn(savedCustomer);

        //when
        CustomerDTO savedDTO = customerService.createCustomer(customerDTO);

        //then
        assertEquals(customerDTO.getFirstname(), savedDTO.getFirstname());
        assertEquals(customerDTO.getLastname(), savedDTO.getLastname());
        assertEquals(customerDTO.getCustomerURL(), savedDTO.getCustomerURL());
    }

    @Test
    public void testSaveCustomerById() {
        //given
        CustomerDTO customerDTO = new CustomerDTO();
        customerDTO.setFirstname(FIRSTNAME);
        customerDTO.setLastname(LASTNAME);
        customerDTO.setCustomerURL("/api/v1/customers/1");

        Customer savedCustomer = new Customer();
        savedCustomer.setFirstname(customerDTO.getFirstname());
        savedCustomer.setLastname(customerDTO.getLastname());;
        savedCustomer.setId(ID1);


        when(customerRepository.save(any(Customer.class))).thenReturn(savedCustomer);

        //when
        CustomerDTO savedDTO = customerService.saveCustomerById(ID1,customerDTO);

        //then
        assertEquals(customerDTO.getFirstname(), savedDTO.getFirstname());
        assertEquals(customerDTO.getLastname(), savedDTO.getLastname());
        assertEquals(customerDTO.getCustomerURL(), savedDTO.getCustomerURL());
    }

    @Test
    public void testDeleteCustomerById() throws  Exception {
        Long id = 1L;

        customerService.deleteCustomerById(id);

        verify(customerRepository,times(1)).deleteById(anyLong());
    }

}