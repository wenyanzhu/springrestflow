package com.bizintelli.restflow.services;

import com.bizintelli.restflow.api.v1.mapper.CategoryMapper;
import com.bizintelli.restflow.api.v1.model.CategoryDTO;
import com.bizintelli.restflow.domain.Category;
import com.bizintelli.restflow.respositories.CategoryRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

public class CategoryServiceImplTest {

    private final String NAME = "Jeo";
    private final long ID = 1L;

    CategoryService categoryService;

    @Mock
    CategoryRepository categoryRepository;

    @Before
    public void setUp() throws Exception{
        MockitoAnnotations.initMocks(this);
        categoryService = new CategoryServiceImpl(CategoryMapper.INSTANCE, categoryRepository);
    }


    @Test
    public void getAllCategories() throws Exception {
        //given
        List<Category> categoryList= Arrays.asList(new Category(), new Category(),new Category());
        when(categoryRepository.findAll()).thenReturn(categoryList);

        //when
        List<CategoryDTO> categoryDTOS=categoryService.getAllCategories();

        //then
        assertEquals(3,categoryDTOS.size());
    }

    @Test
    public void getCategoryByName() throws Exception {
        //given
        Category category = new Category();
        category.setId(ID);
        category.setName(NAME);

        when (categoryRepository.findByName(anyString())).thenReturn(category);

        //when
        CategoryDTO categoryDTO = categoryService.getCategoryByName(NAME);

        //then
        assertEquals(Long.valueOf(ID), categoryDTO.getId());
        assertEquals(NAME,categoryDTO.getName());
    }

}