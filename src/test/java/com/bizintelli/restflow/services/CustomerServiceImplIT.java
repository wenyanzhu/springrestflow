package com.bizintelli.restflow.services;

import com.bizintelli.restflow.api.v1.mapper.CustomerMapper;
import com.bizintelli.restflow.api.v1.model.CustomerDTO;
import com.bizintelli.restflow.bootstrap.Bootstrap;
import com.bizintelli.restflow.domain.Customer;
import com.bizintelli.restflow.respositories.CategoryRepository;
import com.bizintelli.restflow.respositories.CustomerRepository;
import com.bizintelli.restflow.respositories.VendorRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotNull;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsNot.not;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class CustomerServiceImplIT {
    @Autowired
    CustomerRepository customerRepository;
    @Autowired
    CategoryRepository categoryRepository;
    @Autowired
    VendorRepository vendorRepository;

    CustomerService customerService;

    @Before
    public void setUp() throws  Exception {
        System.out.println("Loading Customer Data...");
        System.out.println(customerRepository.findAll().size());

        //Setup for testing
        Bootstrap bootstrap = new Bootstrap(categoryRepository,customerRepository,vendorRepository);
        bootstrap.run();

        customerService = new CustomerServiceImpl(CustomerMapper.INSTANCE, customerRepository);
    }

    @Test
    public void patchCustomerUpdateFirstName() throws Exception {
        String updatedName = "UpdatedName";
        long id = getCustomerByIdValue();

        Customer originalCustomer = customerRepository.getOne(id);
        assertNotNull(originalCustomer);

        String originalFirstName = originalCustomer.getFirstname();
        String originalLastName = originalCustomer.getLastname();

        CustomerDTO customerDTO = new CustomerDTO();
        customerDTO.setFirstname(updatedName);

        customerService.patchCustomerById(id,customerDTO);

        Customer updatedCustomer = customerRepository.findById(id).get();

        assertNotNull(updatedCustomer);
        assertEquals(updatedName,updatedCustomer.getFirstname());
        assertEquals(originalLastName,updatedCustomer.getLastname());
        assertThat(originalFirstName,not(equalTo(updatedCustomer.getFirstname())));
    }


    @Test
    public void patchCustomerUpdateLastName() throws Exception {
        String updatedName = "UpdatedName";
        long id = getCustomerByIdValue();

        Customer originalCustomer = customerRepository.getOne(id);
        assertNotNull(originalCustomer);

        String originalFirstName = originalCustomer.getFirstname();
        String originalLastName = originalCustomer.getLastname();

        CustomerDTO customerDTO = new CustomerDTO();
        customerDTO.setLastname(updatedName);

        customerService.patchCustomerById(id,customerDTO);

        Customer updatedCustomer = customerRepository.findById(id).get();

        assertNotNull(updatedCustomer);
        assertEquals(updatedName,updatedCustomer.getLastname());
        assertEquals(originalFirstName,updatedCustomer.getFirstname());
        assertThat(originalLastName,not(equalTo(updatedCustomer.getLastname())));
    }


    private Long getCustomerByIdValue() {
        List<Customer> customerList = customerRepository.findAll();
        System.out.println("Customer Found:" + customerList.size());
        return customerList.get(0).getId();
    }
}
