package com.bizintelli.restflow.services;

import com.bizintelli.restflow.api.v1.mapper.VendorMapper;
import com.bizintelli.restflow.api.v1.model.VendorDTO;
import com.bizintelli.restflow.api.v1.model.VendorListDTO;
import com.bizintelli.restflow.domain.Vendor;
import com.bizintelli.restflow.respositories.VendorRepository;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class VendorServiceImplTest {

    public static final String VENDORNAME = "NewVendor";
    public static final long VENDERID = 1L;

    @Mock
    VendorRepository vendorRepository;

    VendorService vendorService;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        vendorService = new VendorServiceImpl(vendorRepository, VendorMapper.INSTANCE);
    }

    @Test
    public void getVendorById() throws Exception {

        //given
         Vendor vendor = getVendor();

         given(vendorRepository.findById(anyLong())).willReturn(Optional.of(vendor));

        //when
         VendorDTO vendorDTO = vendorService.getVendorById(VENDERID);

        //then
        then(vendorRepository).should(times(1)).findById(anyLong());

        //Junit with asserts
        assertEquals(Long.valueOf(VENDERID),vendorDTO.getId());
        assertEquals(VENDORNAME, vendorDTO.getName());
    }

    private Vendor getVendor() {
        //given
        Vendor vendor =new Vendor();
        vendor.setId(VENDERID);
        vendor.setName(VENDORNAME);
        return vendor;
    }

    @Test
    public void getAllVendors() throws Exception {
        //given
         List<Vendor> vendorList = Arrays.asList(new Vendor(),new Vendor(),new Vendor());
         when(vendorRepository.findAll()).thenReturn(vendorList);
        //when
         VendorListDTO vendorDTOS = vendorService.getAllVendors();
        //then
        assertEquals(3,vendorDTOS.getVendors().size());
    }

    @Test
    public void createNewVendor() throws Exception {
        //given
         VendorDTO vendorDTO = new VendorDTO();
         vendorDTO.setName(VENDORNAME);
         vendorDTO.setVendorURL(VendorServiceImpl.BASEURL + VENDERID);
         vendorDTO.setId(VENDERID);

         Vendor savedVendor= new Vendor();
         savedVendor.setName(vendorDTO.getName());
         savedVendor.setId(VENDERID);

         when(vendorRepository.save(any(Vendor.class))).thenReturn(savedVendor);

        //when
         VendorDTO savedVendorDTO = vendorService.createNewVendor(vendorDTO);

        //then
        assertEquals(vendorDTO.getName(),savedVendorDTO.getName());
        assertEquals(vendorDTO.getVendorURL(),savedVendorDTO.getVendorURL());
    }

    @Test
    public void saveVendorById() throws Exception {
        //given
        VendorDTO vendorDTO = new VendorDTO();
        vendorDTO.setName(VENDORNAME);
        vendorDTO.setVendorURL(VendorServiceImpl.BASEURL + VENDERID);
        vendorDTO.setId(VENDERID);

        Vendor savedVendor= new Vendor();
        savedVendor.setName(vendorDTO.getName());
        savedVendor.setId(VENDERID);

        when(vendorRepository.save(any(Vendor.class))).thenReturn(savedVendor);

        //when
        VendorDTO savedVendorDTO = vendorService.saveVendorById(VENDERID,vendorDTO);

        //then
        assertEquals(vendorDTO.getName(),savedVendorDTO.getName());
        assertEquals(vendorDTO.getVendorURL(),savedVendorDTO.getVendorURL());
    }


    @Test
    public void patchVendor() throws Exception {
        //given

        //when

        //then
    }

    @Test
    public void deleteVendorById() throws Exception {
        //given
         Long id = 1L;
        //when
         vendorService.deleteVendorById(id);
        //then
        verify(vendorRepository,times(1)).deleteById(anyLong());
    }

}