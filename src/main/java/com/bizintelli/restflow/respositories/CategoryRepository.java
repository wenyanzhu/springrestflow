package com.bizintelli.restflow.respositories;

import com.bizintelli.restflow.domain.Category;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoryRepository extends JpaRepository<Category,Long> {
    Category findByName(String name);
}
