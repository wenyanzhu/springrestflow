package com.bizintelli.restflow;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestflowApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestflowApplication.class, args);
	}
}
