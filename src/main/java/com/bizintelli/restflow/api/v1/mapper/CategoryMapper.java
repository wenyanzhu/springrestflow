package com.bizintelli.restflow.api.v1.mapper;

import com.bizintelli.restflow.api.v1.model.CategoryDTO;
import com.bizintelli.restflow.domain.Category;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

@Mapper
public interface CategoryMapper {

    CategoryMapper INSTANCE = Mappers.getMapper(CategoryMapper.class);

    @Mappings( {
            @Mapping(source = "id", target = "id"),
            @Mapping(source = "name", target = "name")
          }
    )
    CategoryDTO categoryToCategoryDTO(Category category);

}

