package com.bizintelli.restflow.api.v1.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
public class CategoryDTO {
    private Long id;
    private String name;
}
