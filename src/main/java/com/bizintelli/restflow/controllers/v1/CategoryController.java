package com.bizintelli.restflow.controllers.v1;

import com.bizintelli.restflow.api.v1.model.CategoryDTO;
import com.bizintelli.restflow.api.v1.model.CategoryListDTO;
import com.bizintelli.restflow.services.CategoryService;
import io.swagger.annotations.Api;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@Api(value="onlinestore", description="Operations pertaining to category in Online Store")
@RestController
@RequestMapping(CategoryController.BASE_URL)
public class CategoryController {

    public static final String BASE_URL="/api/v1/categories/";

    private  final CategoryService categoryService;

    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

// This is the traditional way to implement in the normal @Controller.
//    @GetMapping
//    public ResponseEntity<CategoryListDTO> getAllCategories() {
//        return  new ResponseEntity<CategoryListDTO>(
//                new CategoryListDTO(categoryService.getAllCategories())
//                ,HttpStatus.OK
//        );
//    }
//
//    @GetMapping("{name}")
//    public ResponseEntity<CategoryDTO> getCategoryByName(@PathVariable String name) {
//        return  new ResponseEntity<CategoryDTO>( categoryService.getCategoryByName(name), HttpStatus.OK);
//    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public  CategoryListDTO  getAllCategories() {
        return  new CategoryListDTO(categoryService.getAllCategories());
    }

    @GetMapping("{name}")
    @ResponseStatus(HttpStatus.OK)
    public  CategoryDTO  getCategoryByName(@PathVariable String name) {
        return  categoryService.getCategoryByName(name);
    }
}
