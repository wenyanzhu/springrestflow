package com.bizintelli.restflow.services;

import com.bizintelli.restflow.api.v1.model.VendorDTO;
import com.bizintelli.restflow.api.v1.model.VendorListDTO;
import com.bizintelli.restflow.domain.Vendor;

import java.util.List;

public interface VendorService {

    VendorDTO getVendorById(Long id);

    VendorListDTO getAllVendors();

    VendorDTO createNewVendor(VendorDTO vendorDTO);

    VendorDTO saveVendorById(Long id, VendorDTO vendorDTO);

    VendorDTO patchVendor(Long id, VendorDTO vendorDTO);

    void deleteVendorById(Long id);

}
