package com.bizintelli.restflow.services;

import com.bizintelli.restflow.api.v1.model.CustomerDTO;

import java.util.List;

public interface CustomerService {
    List<CustomerDTO> getCustomers();
    CustomerDTO getCustomerById(Long id);
    CustomerDTO createCustomer(CustomerDTO customerDTO);
    CustomerDTO saveCustomerById(Long Id, CustomerDTO customerDTO);
    CustomerDTO patchCustomerById(Long Id, CustomerDTO customerDTO);
    void deleteCustomerById(Long Id);
}
