package com.bizintelli.restflow.services;

import com.bizintelli.restflow.api.v1.mapper.CustomerMapper;
import com.bizintelli.restflow.api.v1.model.CustomerDTO;
import com.bizintelli.restflow.domain.Customer;
import com.bizintelli.restflow.respositories.CustomerRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CustomerServiceImpl implements CustomerService {

    public static final String BASEURLCUSTOMER = "/api/v1/customers/";
    private final CustomerMapper customerMapper;
    private final CustomerRepository customerRepository;

    public CustomerServiceImpl(CustomerMapper customerMapper, CustomerRepository customerRepository) {
        this.customerMapper = customerMapper;
        this.customerRepository = customerRepository;
    }


    @Override
    public List<CustomerDTO> getCustomers() {
        return customerRepository.findAll().stream().map(customer-> {
                      CustomerDTO customerDTO = customerMapper.customerToCustomerDTO(customer);
                      customerDTO.setCustomerURL(BASEURLCUSTOMER+ customerDTO.getId());
                      return  customerDTO;
                }).collect(Collectors.toList());
    }

    @Override
    public CustomerDTO getCustomerById(Long id) {
        return  customerRepository.findById(id)
                                  .map(customer-> {
                                                    CustomerDTO customerDTO = customerMapper.customerToCustomerDTO(customer);
                                                    customerDTO.setCustomerURL(BASEURLCUSTOMER+ customerDTO.getId());
                                                    return  customerDTO;})
                                  .orElseThrow(ResourceNotFoundException::new);
    }

    @Override
    public CustomerDTO createCustomer(CustomerDTO customerDTO) {
        return saveAndReturnCustomerDTO(customerDTO);
    }

    private CustomerDTO saveAndReturnCustomerDTO(CustomerDTO customerDTO) {
        Customer customer = customerMapper.customerDtoToCustomer(customerDTO);
        Customer savedCustomer = customerRepository.save(customer);
        CustomerDTO returnedCustomerDto= customerMapper.customerToCustomerDTO(savedCustomer);
        returnedCustomerDto.setCustomerURL(BASEURLCUSTOMER + savedCustomer.getId());
        return returnedCustomerDto;
    }

    @Override
    public CustomerDTO saveCustomerById(Long Id, CustomerDTO customerDTO) {
        CustomerDTO returnCustomerDto = saveAndReturnCustomerDTO(customerDTO);
        return returnCustomerDto;
    }

    @Override
    public CustomerDTO patchCustomerById(Long Id, CustomerDTO customerDTO) {
          return customerRepository.findById(Id)
                                   .map(customer -> {

                                          if(customerDTO.getFirstname() != null ) {
                                              customer.setFirstname(customerDTO.getFirstname());
                                          }

                                          if(customerDTO.getLastname() != null ) {
                                              customer.setLastname(customerDTO.getLastname());
                                          }

                                          return customerMapper.customerToCustomerDTO(customer);

                                      })
                                   .orElseThrow(ResourceNotFoundException::new);
    }

    @Override
    public void deleteCustomerById(Long Id) {
        customerRepository.deleteById(Id);
    }
}
