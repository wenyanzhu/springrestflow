package com.bizintelli.restflow.services;

import com.bizintelli.restflow.api.v1.mapper.VendorMapper;
import com.bizintelli.restflow.api.v1.model.VendorDTO;
import com.bizintelli.restflow.api.v1.model.VendorListDTO;
import com.bizintelli.restflow.domain.Vendor;
import com.bizintelli.restflow.respositories.VendorRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class VendorServiceImpl implements VendorService {

    public static final String BASEURL = "/api/v1/vendor/";
    private VendorRepository vendorRepository;
    private VendorMapper vendorMapper;

    public VendorServiceImpl(VendorRepository vendorRepository, VendorMapper vendorMapper) {
        this.vendorRepository = vendorRepository;
        this.vendorMapper = vendorMapper;
    }

    @Override
    public VendorDTO getVendorById(Long id) {
        return  vendorRepository.findById(id).map(vendor -> {
             VendorDTO vendorDTO = vendorMapper.vendorToVendorDTO(vendor);
             vendorDTO.setVendorURL(getVendorURL(vendor.getId()));
             return  vendorDTO;
        }).orElseThrow(ResourceNotFoundException::new);
    }

    private String getVendorURL(Long id) {
        return BASEURL + id;
    }

    @Override
    public VendorListDTO getAllVendors() {
        List<VendorDTO> vendorDTOList = vendorRepository.findAll().stream().map(vendor -> {
             VendorDTO vendorDTO = vendorMapper.vendorToVendorDTO(vendor);
             vendorDTO.setVendorURL(getVendorURL(vendorDTO.getId()));
             return  vendorDTO;
        }).collect(Collectors.toList());

        return  new VendorListDTO(vendorDTOList);
    }

    @Override
    public VendorDTO createNewVendor(VendorDTO vendorDTO) {
        return  saveAndReturnDTO(vendorDTO);
    }

    private VendorDTO saveAndReturnDTO(VendorDTO vendorDTO) {
        Vendor vendor = vendorMapper.vendorDTOtoVendor(vendorDTO);
        VendorDTO returnVendorDTO = vendorMapper.vendorToVendorDTO(vendorRepository.save(vendor));
        returnVendorDTO.setVendorURL(getVendorURL(vendor.getId()));
        return  returnVendorDTO;
    }
    @Override
    public VendorDTO saveVendorById(Long id, VendorDTO vendorDTO) {
        vendorDTO.setId(id);
        return saveAndReturnDTO(vendorDTO);
    }

    @Override
    public VendorDTO patchVendor(Long id, VendorDTO vendorDTO) {
        return vendorRepository.findById(id).map( vendor -> {
               if(vendorDTO.getName()!=null) {
                   vendor.setName( vendorDTO.getName());
               }
               return  vendorMapper.vendorToVendorDTO(vendor);
        }).orElseThrow(ResourceNotFoundException::new);
    }

    @Override
    public void deleteVendorById(Long id) {
          vendorRepository.deleteById(id);
    }
}
