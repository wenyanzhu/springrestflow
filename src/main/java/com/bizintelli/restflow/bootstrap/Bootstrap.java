package com.bizintelli.restflow.bootstrap;

import com.bizintelli.restflow.domain.Category;
import com.bizintelli.restflow.domain.Customer;
import com.bizintelli.restflow.domain.Vendor;
import com.bizintelli.restflow.respositories.CategoryRepository;
import com.bizintelli.restflow.respositories.CustomerRepository;
import com.bizintelli.restflow.respositories.VendorRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class Bootstrap implements CommandLineRunner {

    private CategoryRepository categoryRepository;
    private CustomerRepository customerRepository;
    private VendorRepository vendorRepository;

    public Bootstrap(CategoryRepository categoryRepository, CustomerRepository customerRepository, VendorRepository vendorRepository) {

        this.categoryRepository = categoryRepository;
        this.customerRepository = customerRepository;
        this.vendorRepository = vendorRepository;
    }

    @Override
    public void run(String... args) throws Exception {
        _loadCategory();
        _loadCustomer();
        _loadVendor();
    }

    private void _loadCustomer() {
        Customer customer1=new Customer();
        customer1.setId(1L);
        customer1.setFirstname("Jeo");
        customer1.setLastname(("Wilson"));

        Customer customer2= new Customer();
        customer2.setId(2L);
        customer2.setFirstname("Michael");
        customer2.setLastname("Jian");

        customerRepository.save(customer1);
        customerRepository.save(customer2);

        System.out.println("Customer Load size:" + customerRepository.count());
    }

    private void _loadCategory() {
        Category fruits =new Category();
        fruits.setName("Fruits");

        Category dried =new Category();
        dried.setName("Dried");

        Category fresh =new Category();
        fresh.setName("Fresh");

        Category exotic =new Category();
        exotic  .setName("Exotic");

        Category nuts =new Category();
        nuts.setName("Nuts");

        categoryRepository.save(fruits);
        categoryRepository.save(dried);
        categoryRepository.save(fresh);
        categoryRepository.save(exotic);
        categoryRepository.save(nuts);

        System.out.println("Category Load size:" + categoryRepository.count());
    }

    private void _loadVendor() {
        Vendor vendor1 = new Vendor();
        vendor1.setName("Vendor 1");

        Vendor vendor2 = new Vendor();
        vendor2.setName("Vendor 2");

        vendorRepository.save(vendor1);
        vendorRepository.save(vendor2);

        System.out.println("Vendor Load size:" + vendorRepository.count());

    }
}
